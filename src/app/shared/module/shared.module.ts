import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "@/core/modules/material.module";
import { GooglePlacesDirective } from "../directives/google-place.directive";
import { AgmCoreModule } from "@agm/core";
import { ShopComponent } from "../components/shop/shop.component";
import { PaginatorComponent } from "../components/shop/paginator/paginator.component";
import { ProductComponent } from "../components/shop/product/product.component";
import { BrowseComponent } from "../components/shop/browse/browse.component";
import { FiltersComponent } from "../components/shop/filters/filters.component";
import { DrawerFiltersComponent } from "../components/shop/drawer-filters/drawer-filters.component";

const COMPONENTS = [
  GooglePlacesDirective,
  ShopComponent,
  PaginatorComponent,
  ProductComponent,
  BrowseComponent,
  FiltersComponent,
  DrawerFiltersComponent
];
const IMPORTS = [];
const SERVICES = [];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDArvx-0lyFe8a76WVdA8H_4MsuzZ8yftc",
      libraries: ["places"]
    })
  ],
  providers: [],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  entryComponents: [
      DrawerFiltersComponent, 
      BrowseComponent
    ]
})
export class SharedModule {}
