export interface Product {
    uuid: string;
    SubCategoryUuid: string;
    company: string;
    name: string;
    description?: string;
    price: string;
    unit: string;
    salePrice?: string;
    saleFinish: Date;
    saleStart: Date;
    imagePath: string;
    quantity?: number;
    saleQuantity: number;
    defaultPrice?: string;
  }
