import { Login } from './login.model';

export interface Register extends Login {
    firstName: string,
    lastName: string,
    email: string,
    password: string
  }
