import { Role } from './role.model';

export class User {
    uuid: string;
    token: string;
    email: string;
    firstName: string;
    lastName: string;
    role: Role;
    imgProfilePath: string | ArrayBuffer;
    city: string;
    floor: number;
    addressStreet: string;
    id_card: string;
    phoneNumber:string;
    apartmentNumber: number;
}
