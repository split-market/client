import { Role } from './role.model';

export interface Login {
    email: string;
    password: string;
    role: Role;
  }
