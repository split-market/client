import { Subcategory } from './subcategory';

export interface Category {
    uuid:string;
    name: string;
    SubCategories: Subcategory[];
  }
