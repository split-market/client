export interface ProductFilter {
  search?: string;
  ShopUuid?: string;
  SubCategoryUuid?: string;
  tagsUuids?: Array<string>;
  limit?: String;
  offset?: string;
  sortBy?: number;
}
