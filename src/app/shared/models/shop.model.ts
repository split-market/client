export interface Shop {
   uuid: string;
   name: string;
   address: string;
   FreeShipping: boolean;
   city: string;
   postalCode: string;
   distributionRange: string;
   phoneNumber: string;
   deliveryTime: string;
   priceShipping: string;
  }
