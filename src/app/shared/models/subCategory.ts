export interface Subcategory {
    CategoryUuid?: string; 
    uuid: string;
    name: string;
  }
