export interface Tag {
    uuid: string;
    name: string;
    checked?: boolean;
  }
