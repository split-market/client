export enum Role {
    Admin = 'admin',
    Customer = 'customer',
    Seller = 'seller',
    Guest = 'guest'
}