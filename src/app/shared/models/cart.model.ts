import { Product } from './product.model';

export enum Status {
  Pending = 'pending',
  Finish = 'finish'
}
export interface Cart {
  products: Product[];
  totalPrice: string;
  quantity: number;
}
