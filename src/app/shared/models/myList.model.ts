
export enum StatusDelivery {
    'active' = 'מתבצע',
    'pending' = 'מתבצע',
    'ends' = 'נשלח'
}

export interface MyList {
    orderNumber: number;
    date: Date;
    statusDelivery: StatusDelivery;
    totalPrice: string;
}