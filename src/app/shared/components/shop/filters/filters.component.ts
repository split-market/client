import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Tag } from '@/shared/models/tags.model';

@Component({
  selector: 'shop-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Input() tags: Tag[];
  @Output() tagsChange = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {}

  /**
   * Checkbox change value
   * toggle tag checked value
   * emit to shop component
   * @param {Tag} tag - tag checked change
   */
  change(tag: Tag): void {
      tag.checked = !tag.checked;
      this.tagsChange.emit(tag);
  }
}
