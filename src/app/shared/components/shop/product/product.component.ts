import { Component, OnInit, Input } from '@angular/core';
import { Product } from '@/shared/models/product.model';

@Component({
    selector: 'shop-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

    @Input() product: Product; // Get product
    @Input() viewAs: string; // View as 'grid' or 'list', change the scss

    constructor(
    ) { }

    ngOnInit() {}
    /**
     * Get sale in Percentage
     * of the sale price from orginal price
     * @return {string} - sale percentage
     */
    getSalePercentage(): string {
        return Math.abs(Math.floor(parseFloat(this.product.salePrice)
            / parseFloat(this.product.price) * 100) - 100).toString();
    };
}
