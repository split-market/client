import { Component, Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material';

@Component({
    selector: 'shop-paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginatorComponent {

    @ViewChild('paginator') paginator: MatPaginator;

    @Input() offset: string;
    @Input() limit: string;
    @Input() productsLength: number;
    @Output() pageEvent = new EventEmitter<Object>();

    pageIndex: number;
    pageSize: number;

    constructor() { }

    getPageIndex(): number {
        return this.offset && parseInt(this.offset) != 0 ? parseInt(this.offset) / parseInt(this.limit) : 0;
    };
    getPageSize(): number {
        return parseInt(this.limit) ? parseInt(this.limit) : 24;
    };

    /**
     * Page change limit or offset 
     * emit to shop components to get new products
     * @param {PageEvent} pageEvent - event when page change
     */
    changePage(pageEvent: PageEvent): void {
        this.pageEvent.emit(pageEvent);
    };
    /**
     * Reset the index page
     */
    public resetPaginator(): void {
        this.paginator.firstPage();
    }
}
