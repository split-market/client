import {
  Component,
  OnInit,
  OnDestroy,
  ContentChild,
  TemplateRef
} from "@angular/core";
import { ShopService } from "@/core/http/shop.service";
import { PageEvent, MatBottomSheet } from "@angular/material";
import { Product } from "@/shared/models/product.model";
import { Subcategory } from "@/shared/models/subcategory";
import { Tag } from "@/shared/models/tags.model";
import {
  finalize,
  map,
  debounceTime,
  distinctUntilChanged,
  mergeMap
} from "rxjs/operators";
import { DrawerFiltersComponent } from "./drawer-filters/drawer-filters.component";
import { SnackbarService } from "@/core/services/snackbar.service";
import { Subscription, Observable, Subject } from "rxjs";
import { AuthenticationService } from "@/core/authentication/authentication.service";
import { BreakpointObserver } from "@angular/cdk/layout";
import { BrowseComponent } from "./browse/browse.component";
import { ActivatedRoute, Router } from "@angular/router";
import { ProductFilter } from "@/shared/models/productFilters.model";
import { FormControl, Validators } from "@angular/forms";
import { CartService } from "@/modules/client/services/cart.service";

@Component({
  selector: "shared-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"]
})
export class ShopComponent implements OnInit, OnDestroy {
  @ContentChild("matCardActions") matCardActionsTmp: TemplateRef<any>;

  searchControl: FormControl = new FormControl(null, [
    Validators.required,
    Validators.minLength(3)
  ]);

  searchTextChanged = new Subject<string>();
  products: Product[] = [];
  tags: Tag[] = [];
  searchOptions: string[] = [];
  sortBy = ["שם: א-ת", "שם: ת-א", "מחיר: נמוך לגבוה", "מחיר: גבוה לנמוך"];
  categoryName: string = "מבצעים קיימים";
  countProducts = 0;
  currentShopUuid: string;
  viewAs: string = "grid"; // View as the product list
  showChips: boolean;
  isLoadProduct: boolean;
  subscription: Subscription;
  filtersQuery: ProductFilter = {};
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(["(max-width: 599.98px)"])
    .pipe(
      map(result => {
        return result.matches;
      })
    );

  constructor(
    private ShopService: ShopService,
    private cartService: CartService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService,
    private authenticationService: AuthenticationService,
    private breakpointObserver: BreakpointObserver,
    private bottomSheet: MatBottomSheet
  ) {
    this.subscription = this.authenticationService.currentUserValue.subscribe(
      () => {
        this.isLoadProduct = false;
        this.setProducts(this.products);
      }
    );
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.currentShopUuid = params.id;
    });

    this.route.queryParams.subscribe(queryParams => {
      this.filtersQuery = queryParams;

      if (queryParams.SubCategoryUuid) {
        this.getTags(queryParams.SubCategoryUuid);
      }
      if (!queryParams.hasOwnProperty("search")) {
        this.getProducts();
      } else {
        this.searchControl.setValue(queryParams.search || "");
        this.search();
      }
    });

    this.searchTextChanged
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        mergeMap(search =>
          this.ShopService.searchAutocomplete(this.currentShopUuid, search)
        )
      )
      .subscribe(res => (this.searchOptions = res.names));
  }
  /**
   * Call shop service to get products by options filters
   * by subcategory, tags, limit, offset
   * if have user - get user cart and compare the products
   */
  getProducts(): void {

    this.isLoadProduct = false;
    this.ShopService.getProductByShop(
      this.currentShopUuid,
      this.filtersQuery || {}
    )
      .pipe(finalize(() => (this.isLoadProduct = true)))
      .subscribe(
        data => {
          this.countProducts = data.count || 0;
          this.setProducts(data.products);
        },
        () => this.snackbarService.openGlobalError()
      );
  }
  /**
   * Compare two array of products
   * check if have the same value
   * set the quantity of cart on shop
   */
  setProducts(products: Product[]): void {
    let cartProducts = this.cartService.cartProductsValue;

    if (!this.authenticationService.isLogin() || !cartProducts.length) {
      this.products = products;
    } else {
      products.map(shopProduct => {
        cartProducts.map(cartProduct => {
          if (shopProduct.uuid === cartProduct.uuid) {
            shopProduct.quantity = cartProduct.quantity;
          }
        });
      });
    }
    this.products = products;
    this.isLoadProduct = true;
  }
  /**
   * TODO
   * @param {Subcategory} - subcategory emit from child
   */
  subcategoryChange(subcategory: Subcategory): void {
    this.categoryName = subcategory.name;

    this.filtersQuery = {
      SubCategoryUuid: subcategory.uuid
    };
    this.searchControl.setValue("");

    this.router.navigate([], { queryParams: { ...this.filtersQuery } });
  }
  /**
   * change the checked of tag to false
   * set on tags
   * @param {Tag} tag - tag to remove the checked
   */
  removeTag(tag: Tag): void {
    var tags = this.tags;
    tags.map(t => {
      if (t.uuid === tag.uuid) t.checked = false;
    });
    this.tagsChange(tag);
  }
  /**
   * Get http req of tags by subcategory
   * @param {Subcategory} subcategory - subcategory selected
   */
  getTags(subcategoryUuid: string): void {
    this.ShopService.getTags(subcategoryUuid).subscribe(
      tags => {
        let tagsUuids = this.filtersQuery.tagsUuids;

        if (tagsUuids && tagsUuids.length) {
          tagsUuids = Array.isArray(tagsUuids)
            ? tagsUuids
            : (tagsUuids = (tagsUuids as any).split(","));
          this.filtersQuery = { ...this.filtersQuery, tagsUuids };
        }

        if (tagsUuids && tagsUuids.length) {
          this.showChips = true;
          tags.map(tag => {
            if (tagsUuids.includes(tag.uuid)) {
              tag.checked = true;
            }
          });
        } else {
          this.showChips = false;
        }
        this.tags = tags;
      },
      () =>
        this.snackbarService.open(
          "אין אפשרות להציג תגיות לפילטור, נסה שוב מאוחר יותר",
          "error"
        )
    );
  }
  /**
   * TODO
   * @param {Tag} tag
   */
  tagsChange(tag: Tag): void {
    let uuids = this.filtersQuery.tagsUuids || [];

    tag.checked
      ? uuids.push(tag.uuid)
      : (uuids = uuids.filter(uuid => uuid != tag.uuid));

    this.filtersQuery = { ...this.filtersQuery, tagsUuids: uuids };

    this.router.navigate([], { queryParams: { ...this.filtersQuery } });
  }
  /**
   * Get limit and offset from Paginator component
   * Set limit and offset on product filter
   * call get new products func
   * @param {PageEvent} page - Event of paginator
   */
  paginatorChange(page: PageEvent): void {
    this.filtersQuery = {
      ...this.filtersQuery,
      limit: page.pageSize.toString(),
      offset: (page.pageIndex * page.pageSize).toString()
    };
    this.router.navigate([], { queryParams: { ...this.filtersQuery } });
  }
  /**
   * Set sort by as index of products filters
   * and call function get new products sort by the user select
   * @param {number} index - Index of sort by
   */
  sortByChange(index: number): void {
    this.filtersQuery = { ...this.filtersQuery, sortBy: index };
    this.router.navigate([], { queryParams: { ...this.filtersQuery } });
  }
  /**
   * Open Drawer categories
   * on small breakpoint
   */
  openDrawerCategories(): void {
    this.bottomSheet
      .open(BrowseComponent, {
        autoFocus: false,
        restoreFocus: false,
        data: { subcategory: this.filtersQuery.SubCategoryUuid }
      })
      .afterDismissed()
      .subscribe(subcategory => {
        if (subcategory) {
          this.subcategoryChange(subcategory);
        }
      });
  }
  /**
   * Open DrawerFilters
   * on small breakpoint
   */
  openDrawerFilters(): void {
    this.bottomSheet
      .open(DrawerFiltersComponent, {
        autoFocus: false,
        restoreFocus: false,
        data: { tags: this.tags }
      })
      .afterDismissed()
      .subscribe(data => {
        if (data && data.tags) {
          this.tagsChange(data.tags);
        }
      });
  }

  search(): void {
    if (this.searchControl.invalid) return;

    let term = this.searchControl.value;
    this.filtersQuery = { search: term };

    this.ShopService.search(this.currentShopUuid, term).subscribe(
      data => {
        this.countProducts = data.count || 0;
        this.setProducts(data.products);
      },
      () => this.snackbarService.openGlobalError()
    );

    this.router.navigate(["."], {
      queryParams: { ...this.filtersQuery },
      relativeTo: this.route
    });
  }

  input(): void {
    if (this.searchControl.invalid) {
      this.searchOptions = [];
      return;
    }
    this.searchTextChanged.next(this.searchControl.value);
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }
}
