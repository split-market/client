import { Component, OnInit, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { Tag } from '@/shared/models/tags.model';

@Component({
  selector: 'shop-drawer-filters',
  templateUrl: './drawer-filters.component.html',
  styleUrls: ['./drawer-filters.component.scss']
})
export class DrawerFiltersComponent implements OnInit {

  tags: Tag[];

  constructor(
    private matBottomSheetRef: MatBottomSheetRef<DrawerFiltersComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
  ) {
    if (data.tags.length) {
      this.tags = data.tags;
    }
  }

  ngOnInit() {}

  /**
   * Close buttom sheet window
   */
  close(): void {
    this.matBottomSheetRef.dismiss();
  };
  /**
   * Any checkbox change - close bottom sheet with new tags
   * @param {Tag[]} tags
   */
  tagsChange(tags: Tag[]): void {
    this.matBottomSheetRef.dismiss({subcategory: null, tags: tags}); 
  };
}
