import { Component, Output, EventEmitter, Input, Inject, Optional, OnInit, ViewChild } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { of as observableOf, Subscription } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ShopService } from '@/core/http/shop.service';
import { MatDrawer, MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material';
import { Subcategory } from '@/shared/models/subcategory';
import { SnackbarService } from '@/core/services/snackbar.service';
import { Category } from '@/shared/models/category.model';

export interface FileNode {
    name: string;
    uuid: string;
    SubCategories?: FileNode[];
}

export interface FlatTreeNode {
    uuid: string;
    name: string;
    level: number;
    expandable: boolean;
}

@Component({
    selector: 'shop-browse',
    templateUrl: './browse.component.html',
    styleUrls: ['./browse.component.scss'],
})
export class BrowseComponent implements OnInit {

    @ViewChild('tree') tree;

    @Input() drawer: MatDrawer;
    @Input() subCategoryUuid: string;
    @Output() subcategoryChange = new EventEmitter<Subcategory>();
    @Output() setCategoryName = new EventEmitter<string>();

    currentSubcategoryUuid: string;
    treeControl: FlatTreeControl<FlatTreeNode>;
    treeFlattener: MatTreeFlattener<FileNode, FlatTreeNode>;
    dataSource: MatTreeFlatDataSource<FileNode, FlatTreeNode>;
    subscription: Subscription;

    constructor(
        private ShopService: ShopService,
        private snackbarService: SnackbarService,
        @Optional() private matBottomSheetRef: MatBottomSheetRef<BrowseComponent>,
        @Optional() @Inject(MAT_BOTTOM_SHEET_DATA) public bottomSheetData: any
    ) {

        this.treeFlattener = new MatTreeFlattener(
            this.transformer,
            this.getLevel,
            this.isExpandable,
            this.getSubCategories
        );

        this.treeControl = new FlatTreeControl(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

        this.subscription =
            this.ShopService.getCategories()
                .subscribe(categories => {
                    this.dataSource.data = categories;
                    if (this.currentSubcategoryUuid) {
                        this.emitCategoryName(categories);
                    }
                    this.tree.treeControl.expandAll();
                },
                    () => this.snackbarService.open("לא ניתן להציג את הקטגוריות, נסה שוב מאוחר יותר", 'error'));
    };
    ngOnInit() {
        this.currentSubcategoryUuid = this.subCategoryUuid;
    }
    emitCategoryName(categories: Category[]): any {

        findName:
        for (const key in categories) {
            for (const key2 in categories[key].SubCategories) {
                if (categories[key].SubCategories[key2].uuid === this.currentSubcategoryUuid) {
                    this.setCategoryName.emit(categories[key].SubCategories[key2].name);
                    break findName;
                }
            }
        }
    }
    /**
    * Transform the data to something the tree can read.
    */
    transformer(node: FileNode, level: number) {
        return {
            name: node.name,
            level: level,
            expandable: !!node.SubCategories,
            uuid: node.uuid
        };
    };
    /**
    * Get the level of the node
    */
    getLevel(node: FlatTreeNode) {
        return node.level;
    };
    /**
    * Get whether the node is expanded or not.
    */
    isExpandable(node: FlatTreeNode) {
        return node.expandable;
    };
    /**
    * Get whether the node has subCategories or not.
    */
    hasChild(index: number, node: FlatTreeNode) {
        return node.expandable;
    };
    /**
    * Get the subCategories for the node.
    */
    getSubCategories(node: FileNode) {
        return observableOf(node.SubCategories);
    };
    /**
     * Subcategory change
     * set the selected subcategory 
     * emit to shop components to get new products
     * if is in bottom sheet - close with subcategory
     */
    changeSubcategory(subCategory: Subcategory): void {
        this.currentSubcategoryUuid = subCategory.uuid;
        this.subcategoryChange.emit(subCategory);
        if (this.bottomSheetData) {
            this.matBottomSheetRef.dismiss(subCategory);
        }
    };
    /**
     * Dismiss the bottom sheet dialog
     * When breakpoint sm
     */
    close(): void {
        this.matBottomSheetRef.dismiss();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    };
}
