import { Directive, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader } from '@agm/core';

declare const google: any;

@Directive({
  selector: '[google-place]'
})
export class GooglePlacesDirective implements OnInit {

  private element: HTMLInputElement;
  @Output() onSelect = new EventEmitter<string>();

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private elRef: ElementRef,
  ) {
    this.element = this.elRef.nativeElement;
  }
  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.element, {
        types: ['address'],
        componentRestrictions: {'country': 'IL'}
      });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        this.onSelect.emit(autocomplete.getPlace());
      });
    });
  }
}