import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ClientRoutingModule } from "./client-routing.module";
import { MaterialModule } from "@/core/modules/material.module";
import { SharedModule } from "@/shared/module/shared.module";

import { NavComponent } from "./components/nav/nav.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HomeComponent } from "./components/home/home.component";
import { ClientComponent } from "./pages/client.component";
import { CartComponent } from "./components/cart/cart.component";
import { ToolbarComponent } from "./components/nav/toolbar/toolbar.component";
import { RegisterComponent } from "./components/sign-in/register/register.component";
import { SignInComponent } from "./components/sign-in/sign-in.component";
import { LoginComponent } from "./components/sign-in/login/login.component";
import { MyListsComponent } from "./components/my-lists/my-lists.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { MyListComponent } from "./components/my-lists/my-list/my-list.component";
import { ContactUsComponent } from "./components/contact-us/contact-us.component";
import { CheckoutComponent } from "./components/checkout/checkout.component";
import { ChooseShopComponent } from "./components/choose-shop/choose-shop.component";
import { ShoppingComponent } from "./components/shopping/shopping.component";
import { BtnQuantityComponent } from './components/btn-quantity/btn-quantity.component';
import { AddCartComponent } from './components/add-cart/add-cart.component';

@NgModule({
  declarations: [
    ClientComponent,
    HomeComponent,
    NavComponent,
    FooterComponent,
    CartComponent,
    ToolbarComponent,
    SignInComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    MyListsComponent,
    MyListComponent,
    ContactUsComponent,
    CheckoutComponent,
    ChooseShopComponent,
    BtnQuantityComponent,
    ShoppingComponent,
    AddCartComponent
  ],
  providers: [],
  imports: [
      CommonModule, 
      ClientRoutingModule, 
      MaterialModule, 
      SharedModule
    ],
  entryComponents: [
      SignInComponent, 
      ChooseShopComponent
    ]
})
export class ClientModule {}
