import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Product } from '@/shared/models/product.model';
import { Status, Cart } from '@/shared/models/cart.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment.prod';
import { MyList } from '@/shared/models/myList.model';
import { tap } from 'rxjs/operators';
import { Location } from '@/shared/models/Location.model';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    private cartProductsSubject: BehaviorSubject<Product[]>;
    public cartProducts: Observable<Product[]>;

    constructor(
        private http: HttpClient
    ) {
        this.cartProductsSubject = new BehaviorSubject<Product[]>([]);
        this.cartProducts = this.cartProductsSubject.asObservable();
    }
    /**
     * @return {Cart} - current quantity and totalPrice
     */
    public get cartProductsValue(): Product[] {
        return this.cartProductsSubject.value;
    };
    /**
     * Set new products on subject
     * @param {Product[]} products - new products array
     */
    public setCartProductsValue(products: Product[]): void {
        this.cartProductsSubject.next(products);
    };
    /*
    * Get cart products of user
    * @param {Status} status - status - of cart
    */
    getCart(status: Status): Observable<Product[]> {
        const href = `${environment.cart}`;
        return this.http.get<Product[]>(href, { params: { status } })
            .pipe(tap((cartProducts => this.cartProductsSubject.next(cartProducts))));
    };
    /**
    * Add new product to cart and update the cart data subject
    * @param {Product} product - new product item add to cart
    * @return {Product} - the new product
    */
    addCartProduct(data: Product | Array<object>): Observable<Product> {
        const href = `${environment.cart}`;
        return this.http.post<Product>(href,
            Array.isArray(data) ? { products: data } : { productUuid: (data as Product).uuid })
            .pipe(tap(() => {
                if (!Array.isArray(data)) {
                    let newCartProducts = this.cartProductsValue;
                    newCartProducts.push(data);
                    this.cartProductsSubject.next(newCartProducts);
                }
            }));
    };
    /**
    * Update product in cart
    * @param {Product} product - product item update the quantity
    * @return {object} - of success or failed to update the product
    */
    updateCartProduct(product: Product): Observable<object> {
        const href = `${environment.cart}`;
        return this.http.put<Object>(href, product)
            .pipe(tap(() => {
                let newCartProducts = this.cartProductsValue;
                newCartProducts.map(p => { if (p.uuid === product.uuid) p.quantity = product.quantity });
                this.cartProductsSubject.next(newCartProducts);
            }));
    };

    /**
    * Remove product from cart and update the quantity and total price
    * @param {string} productUuid - Uuid of product to remove form cart
    * @return {object} - of success or failed to update the product
    */
    removeCartProduct(productUuid: string): Observable<Object> {
        const href = `${environment.cart}`;
        return this.http.delete<string>(href, { params: { productUuid } })
            .pipe(tap(() => {
                let newCartProducts = this.cartProductsValue;
                newCartProducts = newCartProducts.filter(p => p.uuid != productUuid);
                this.cartProductsSubject.next(newCartProducts);
            }));
    };
    /**
    * Remove product from cart and update the quantity and total price
    * @param {string} productUuid - Uuid of product to remove form cart
    * @return {object} - of success or failed to update the product
    */
    removeCartAllProduct(): Observable<Object> {
        const href = `${environment.cart}`;
        return this.http.delete<string>(href)
            .pipe(tap(() => this.cartProductsSubject.next([])));
    };
    /**
    * Update prodcuct in cart of user
    * @param {number} quantity - Quantity of product
    */
    updateMyList(status: string, totalPrice: string, date: Date): Observable<object> {
        const href = `${environment.myList}`;
        return this.http.put<Object>(href, { status, totalPrice, date });
    };
    /**
    * Update product in cart of user
    * @param {number} quantity - Quantity of product
    */
   calculateMyList(UserUuid: string, location?: Location): Observable<object> {
        const href = `${environment.myListCalculate}`;
        return this.http.get<Object>(href, { params: Object.assign({}, {UserUuid} , location)});
    };
    /**
    * Get all history lists of cart
    * @return {Observable<MyList[]>} - my lists history of user cart
    */
    getMyLists(): Observable<MyList[]> {
        const href = `${environment.myList}`;
        return this.http.get<MyList[]>(href);
    };
    /**
    * Get products list by uuid
    * @param {string} uuid - uuid of list
    * @return {Observable<Product[]>} products of this list
    */
    getListById(uuid: string): Observable<Product[]> {
        const href = `${environment.myList}`;
        return this.http.get<Product[]>(href + `/${uuid}`);
    };
}
