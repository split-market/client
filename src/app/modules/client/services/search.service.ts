import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { environment } from 'environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

    constructor(
        private http: HttpClient
    ) { }

    /**
     * @param {Observable<string>} terms - the string to search
     * take debounceTime of search call service
     */
    search(terms: Observable<string>) {
      return terms.pipe(debounceTime(400),
      switchMap(term => this.searchEntries(term)));
    }
    /**
     * @param {string} term - term to search
     * @return - array of sarch results
     */
    searchEntries(term: string) {
      return this.http.get(environment.search + term);
    }
  }