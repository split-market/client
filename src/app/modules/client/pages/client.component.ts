import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@/core/authentication/authentication.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() { 
    this.authenticationService.setCurrentUserValue(JSON.parse(localStorage.getItem('currentUser')));
  }
}
