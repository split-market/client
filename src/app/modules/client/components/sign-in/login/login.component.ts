import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Login } from '@/shared/models/login.model';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { finalize } from 'rxjs/operators';
import { Role } from '@/shared/models/role.model';

@Component({
    selector: 'signin-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    @Output() closeDialog = new EventEmitter();

    loginForm: FormGroup;
    isInvalidLogin: boolean;
    spinnerBtn: boolean;

    constructor(
        private fb: FormBuilder,
        private authenticationService: AuthenticationService
    ) {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            role: [Role.Customer]
        });
    }

    ngOnInit() { }

    /**
     * Chck if form is valid and is not in load req mode
     * show spinner load and send req to service
     * when user login close the dialog
     */
    login(): void {
        if (this.loginForm.invalid || this.spinnerBtn) return;

        this.isInvalidLogin = false;
        this.spinnerBtn = true;

        const user: Login = this.loginForm.value;
        this.authenticationService.login(user)
            .pipe(finalize(() => this.spinnerBtn = false))
            .subscribe(() => this.closeDialog.emit(),
                () => this.isInvalidLogin = true);
    };
}
