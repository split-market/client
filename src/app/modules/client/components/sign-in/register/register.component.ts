import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { finalize } from 'rxjs/operators';
import { Register } from '@/shared/models/register.model';
import { Role } from '@/shared/models/role.model';

const REGEX_PASSWORD = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$";

@Component({
    selector: 'signin-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    @Output() closeDialog = new EventEmitter;

    registerForm: FormGroup;
    spinnerBtn: boolean;
    isInvalidRegister: boolean;

    constructor(
        private fb: FormBuilder,
        private authenticationService: AuthenticationService
    ) {
        this.registerForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(2)]],
            lastName: ['', [Validators.required, Validators.minLength(2)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern(REGEX_PASSWORD)]],
            role: [Role.Customer]
        });
    }
    ngOnInit() { }
    /**
     * User register
     * check if have guest user - update the user to Customer
     * if clean the current user from storage create new user
     */
    register(): void {
        if (this.registerForm.invalid || this.spinnerBtn) return;

        this.isInvalidRegister = false;
        this.spinnerBtn = true;

        const registerForm: Register = this.registerForm.value;
        const currentUser = this.authenticationService.getUser();

        // Check if have current user and type role guest - update the user role 
        if (currentUser && currentUser.role === Role.Guest) {
            this.authenticationService.update(registerForm)
                .subscribe(() => this.closeDialog.emit(),
                    () => {
                        this.isInvalidRegister = true;
                        this.spinnerBtn = false;
                    }); 
        } else {
            this.authenticationService.register(registerForm)
                .pipe(finalize(() => this.spinnerBtn = false))
                .subscribe(() => this.closeDialog.emit(),
                    (err) => {
                        console.log(err)
                        this.isInvalidRegister = true;
                        this.spinnerBtn = false;
                    });
        }
    };
}
