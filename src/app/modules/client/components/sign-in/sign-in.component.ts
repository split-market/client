import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'clint-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  
  selected: number = 0;
  showSkipBtn: boolean;

  constructor(
    private dialogRef: MatDialogRef<SignInComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.selected = data.selected;
    this.showSkipBtn = data.showSkipBtn ? data.showSkipBtn : null;

  }

  ngOnInit() {}

  /**
   * Close sign in dialog
   */
  closeDialog(): void {
    this.dialogRef.close();
  };
  /**
   * Tab index change set index on current index
   * @param {number} index index of tab in group
   */
  selectedIndexChange(index: number): void {
      this.selected = index;
  };
}
