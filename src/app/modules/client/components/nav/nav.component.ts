import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { User } from '@/shared/models/user.model';
import { SnackbarService } from '@/core/services/snackbar.service';
import { Role } from '@/shared/models/role.model';
import { SignInComponent } from '../sign-in/sign-in.component';

@Component({
    selector: 'client-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit, OnDestroy {

    currentUser: User;
    subscription: Subscription;
    isLoadUser: boolean;

    isHandset$: Observable<boolean> =
        this.breakpointObserver.observe(['(max-width: 767.98px)'])
            .pipe(map(result => {
                return result.matches;
            }));

    constructor(
        private dialog: MatDialog,
        private breakpointObserver: BreakpointObserver,
        private authenticationService: AuthenticationService,
        private snackbarService: SnackbarService
    ) {
        this.subscription = this.authenticationService.currentUserValue
            .subscribe(currentUser => {
                this.currentUser = currentUser && currentUser.role === Role.Customer ? currentUser : null;
                this.isLoadUser = true;
            },
                () => this.snackbarService.openGlobalError());
    }

    ngOnInit() { }

    /**
     * Open dialog sign in
     * @param {number} index Index of tab on dialog signin
     */
    openDialogSignIn(index: number): void {
        this.dialog.open(SignInComponent, {
            autoFocus: false,
            disableClose: true,
            panelClass: 'full-dialog',
            data: { selected: index }
        });
    };
    /**
     * Logout, call authentication service
     * remove user from storage
     */
    logout(): void {
        this.isLoadUser = false;
        this.authenticationService.logout();
    };
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
};