import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { CartService } from '@/modules/client/services/cart.service';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatDrawer } from '@angular/material';
import { Cart } from '@/shared/models/cart.model';
import { User } from '@/shared/models/user.model';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { Product } from '@/shared/models/product.model';

@Component({
    selector: 'nav-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {

    @Output() openDialogSignIn = new EventEmitter();
    @Output() logoutEmitter = new EventEmitter();
    @Input() drawer: MatDrawer; // Get drawer from parent
    @Input() currentUser: User; // Get drawer from parent
    @Input() isLoadUser: boolean; // Get if get user is load

    subscription: Subscription;
    cartData: Cart;
    quantity: number;
    totalPrice: string;


    isHandset$: Observable<boolean> =
        this.breakpointObserver.observe(['(max-width: 767.98px)'])
            .pipe(map(result => {
                return result.matches;
            }));

    constructor(
        private breakpointObserver: BreakpointObserver,
        private authenticationService: AuthenticationService,
        private cartService: CartService
    ) {
        this.subscription =
            this.authenticationService.currentUserValue.subscribe(currentUser => {
                if (currentUser) {
                    this.cartService.cartProducts.subscribe(cartProducts => this.setQuantityAndTotalPrice(cartProducts));
                } else {
                    this.quantity = null;
                    this.totalPrice = null;
                }
            });

    }

    ngOnInit() {}

    /**
    * Initialize the quantity and total price of cart
    * if cart is empty or remove clear set null value
    */
    setQuantityAndTotalPrice(cartProducts: Product[]): void {
        if (cartProducts.length) {
            let quantity: number = 0;
            let totalPrice: string = "0";
            cartProducts.map(product => {
                quantity += product.quantity;
                totalPrice = (parseFloat(totalPrice) + (product.quantity * parseFloat(product.price))).toFixed(2).toString();
            });
            this.quantity = quantity;
            this.totalPrice = totalPrice;
        } else {
            this.quantity = null;
            this.totalPrice = null;
        }
    };
    /**
     * Emit to nav components func logout
     */
    logout(): void {
        this.logoutEmitter.emit(true);
    };
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
