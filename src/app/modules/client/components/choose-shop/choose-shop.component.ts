import { Component, OnInit, ViewChild } from '@angular/core';
import { SnackbarService } from '@/core/services/snackbar.service';
import { FormControl, Validators } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { MatDialogRef, MatStepper, MatInput } from '@angular/material';
import { Location } from '@/shared/models/Location.model';
import { ShopService } from '@/core/http/shop.service';
import { Shop } from '@/shared/models/shop.model';
import { Router } from '@angular/router';

declare const google: any;

@Component({
  selector: 'app-choose-shop',
  templateUrl: './choose-shop.component.html',
  styleUrls: ['./choose-shop.component.scss']
})
export class ChooseShopComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('addressInput') addressInput: MatInput;

  addressControl: FormControl = new FormControl('', Validators.required);

  shopsList: Shop[] = [];
  shopUuidSelected: string;
  latlng: Location;
  checkValidAddress: boolean;
  isLoadGeolocation: boolean;
  geocoder: any;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private snackbarService: SnackbarService,
    private shopService: ShopService,
    private router: Router,
    private dialogRef: MatDialogRef<ChooseShopComponent>
  ) { }

  ngOnInit() {
    this.mapsAPILoader.load()
      .then(() => this.geocoder = new google.maps.Geocoder)
      .catch(() => this.snackbarService.openGlobalError());
  }

  /** Get current location of user from geolocation */
  getCurrentLocation(event: Event): void {
    event.stopPropagation();
    navigator.geolocation.getCurrentPosition(
      this.tryGetLocation.bind(this),
      this.onGetPositionError.bind(this),
      { enableHighAccuracy: true, timeout: 5000 }
    );
  };
  /* Cant get current lcoation if its disabled for example */
  onGetPositionError() {
    this.snackbarService.open('אין הרשאה למיקום שלך, אפשר מיקום ונסה שוב', 'error');
  }
  /**
   * Try to get address by lat & lng of user from geolocation
   * @param  position - Object of coords lat and lng of user current location 
   */
  tryGetLocation(position: any) {

    this.addressControl.disable();
    this.isLoadGeolocation = true;

    this.latlng = { lat: position.coords.latitude, lng: position.coords.longitude };
    this.geocoder.geocode({ 'location': this.latlng }, results => {
      this.addressControl.enable();
      this.isLoadGeolocation = false;
      results[0] ? this.addressControl.setValue(results[0].formatted_address)
        : this.snackbarService.open('אין אפשרות לקבל את המיקום שלך, נסה שוב מאוחר יותר', 'error');
    });
  };

  /** Set latlng when google places selected */
  setLocation(res: any): void {
    if (res && res.geometry) {
      this.addressControl.setValue(res.formatted_address);
      this.latlng = {
        lat: res.geometry.location.lat(),
        lng: res.geometry.location.lng()
      };
    } else {
      this.addressControl.setErrors({ invalidAddress: true });
    }
  };
  /**
   * Get shops list by location
   * check if is valid address from google maps
   */
  getShopsByLocation(event: Event): void {

    event.preventDefault();
    event.stopPropagation();
    this.checkValidAddress = true;

    const address = this.addressControl.value;
    this.geocoder.geocode({
      'address': address
    }, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK && results.length > 0) {
        this.setLocation(results[0]);
        this.shopService.getShopByLocation(this.latlng)
          .subscribe(shops => {
            if (shops.length) this.shopsList = shops;
            this.stepper.next();
          }, () => this.snackbarService.open('אין אפשרות להציג חנויות כרגע, נסה שוב מאוחר יותר', 'error'));
      } else {
        this.addressControl.setErrors({ invalidAddress: true });
      }
      this.checkValidAddress = false;
    });
  };
  navigateToShop(): void {
    if (this.shopUuidSelected) {
      this.closeDialog();
      this.router.navigate(['/shop', this.shopUuidSelected]);
    }
  }
  /** Close Dialog */
  closeDialog(): void {
    this.dialogRef.close();
  };
  /** Reset stepper */
  reset(): void {
    this.shopsList = [];
    this.latlng = null;
    this.shopUuidSelected = null;
    this.stepper.reset();
  };
}