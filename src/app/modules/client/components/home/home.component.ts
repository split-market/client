import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ChooseShopComponent } from '../choose-shop/choose-shop.component';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(
        private dialog: MatDialog
    ) { }

    ngOnInit() { }

    /**
     * Open dialog of choose shop components
     */
    startShop(): void {
        this.dialog.open(ChooseShopComponent, {
            disableClose: true,
            autoFocus: false,
            panelClass  : ['full-dialog', 'choose-shop'],
        });
    };
}
