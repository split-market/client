import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  
  constructor(
    private fb: FormBuilder,
) {
    this.contactUsForm = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
        message: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]],
        email: ['', [Validators.required, Validators.email]]
    });
}
  ngOnInit() {
  }

  send(){

  }
}
