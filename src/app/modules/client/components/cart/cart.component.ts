import { Component, OnInit, OnDestroy } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { SnackbarService } from '@/core/services/snackbar.service';
import { map } from 'rxjs/operators';
import { Product } from '@/shared/models/product.model';
import { Subscription, Observable } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Role } from '@/shared/models/role.model';
import { User } from '@/shared/models/user.model';
import { SignInComponent } from '../sign-in/sign-in.component';
import { ChooseShopComponent } from '../choose-shop/choose-shop.component';

@Component({
    selector: 'shop-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {

    cartProducts: Product[];
    isLoadCart: boolean;
    isLoadUser: boolean;
    isLoadTotalPrice: boolean;
    subscription: Subscription;
    totalPrice: string;
    user: User;
    displayedColumns = ['product', 'price', 'quantity'];

    isHandset$: Observable<boolean> =
        this.breakpointObserver.observe(['(max-width: 767.98px)'])
            .pipe(map(result => {
                return result.matches;
            }));

    constructor(
        private cartService: CartService,
        private snackbarService: SnackbarService,
        private authenticationService: AuthenticationService,
        private dialog: MatDialog,
        private router: Router,
        private breakpointObserver: BreakpointObserver
    ) {
        // this.subscription =
        //     this.authenticationService.currentUserValue.subscribe((user) => {
        //         this.user = user;
        //         this.cartProducts = this.cartService.cartProductsValue;
        //     });
    };
    ngOnInit() {
        this.cartProducts = this.cartService.cartProductsValue;
        this.isLoadCart = true;
    }
    /**
     * Get total price string with to digit after zero
     * @return {string} of total price of cart with two digits after decimal point
     */
    getTotalPrice(): string {
        this.totalPrice = "0";
        this.cartProducts.map(p => this.totalPrice =
            (parseFloat(this.totalPrice) + (p.quantity * parseFloat(p.price))).toFixed(2).toString());
        return this.totalPrice;
    };
    /**
     * Remove product form local products Array 
     * @param product - product to remove from array
     */
    removeProduct(product: Product): void {
        this.cartProducts = this.cartProducts.filter(p => p.uuid != product.uuid);
    };
    /**
     * Remove product from serve cart of user
     * @param product - product to remove from server 
     */
    removeCartProduct(product: Product): void {
        this.cartService.removeCartProduct(product.uuid)
            .subscribe(() => {
                this.removeProduct(product);
                this.snackbarService.open('המוצר הוסר מהעגלה.', 'success', 1000);
            }, () => this.snackbarService.open('אין אפשרות למחוק את המוצרים שביקשת, נסה שוב מאוחר יותר', 'error'));
    };
    /* Remove all products from cart */
    removeAll(): void {
        this.cartService.removeCartAllProduct()
            .subscribe(() => {
                this.cartProducts = [];
                this.snackbarService.open('העגלה הוסרה בהצלחה', 'success');
            }, () => this.snackbarService.open('אין אפשרות למחוק את סל הקניות שלך, נסה שוב מאוחר יותר'));
    };
    /**
     * open dialog of choose shop component
     * to start shopping
     */
    startShop(): void {
        this.dialog.open(ChooseShopComponent, {
            disableClose: true,
            autoFocus: false,
            panelClass  : ['full-dialog', 'choose-shop'],
        });
    };
    /**
     * Check role of user if is guest
     * open signin dialog 
     * if not navigate to checkout 
     */
    checkout() {

        // If have user and is guest role 
        if (this.authenticationService.getUser()
            && this.authenticationService.getUserType() === Role.Guest) {
            this.dialog.open(SignInComponent, {
                autoFocus: false,
                disableClose: true,
                panelClass: 'sign-in-full-dialog',
                data: { selected: 0 }
            }).afterClosed().subscribe(() => {
                // If the user is now Customer if is login or register
                if (this.authenticationService.getUser()
                    && this.authenticationService.getUserType() === Role.Customer)
                    this.router.navigate(['checkout']);
            });
        } else {
            this.router.navigate(['checkout']);
        }
    };
    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }
};