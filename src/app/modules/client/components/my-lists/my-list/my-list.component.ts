import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '@/modules/client/services/cart.service';
import { Product } from '@/shared/models/product.model';
import { SnackbarService } from '@/core/services/snackbar.service';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { finalize, map } from 'rxjs/operators';

@Component({
  selector: 'lists-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss']
})
export class MyListComponent implements OnInit {

  dataSource: MatTableDataSource<Product>;
  selection = new SelectionModel<Product>(true, []);
  displayedColumns: string[] = ['select', 'product', 'price', 'quantity'];
  uuid: string;
  spinnerBtn: boolean;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private snackbarService: SnackbarService
  ) {
    this.route.params.subscribe(value => {
      this.uuid = value.id;
      this.cartService.getListById(value.id)
        .subscribe(products => {
          this.dataSource = new MatTableDataSource(products);
        },
          () => this.snackbarService.open('אין אפשרות להציג את הרשימה, נסה שוב מאוחר יותר', 'error'));
    });
  };

  ngOnInit() { }

  /**
   * Return boolean if find uuid of product in cart products
   * @param {Product} row - row of product
   */
  exsitInCart(row: Product): boolean {
    return this.cartService.cartProductsValue.some(p => p.uuid === row.uuid);
  };
  /** 
  * Whether the number of selected elements matches the total number of rows. 
  * @return {boolean} - if the number of selected and rows is equal
  */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    var numRows: number = 0;
    this.dataSource.data.map(row => { if (!this.exsitInCart(row)) numRows++ });
    return numSelected === numRows;
  };

  /** 
  * Selects all rows if they are not all selected; otherwise clear selection. 
  */
  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => { if (!this.exsitInCart(row)) this.selection.select(row) });
  };
  /**
   * Add new product to user cart if success 
   * show success message if error show error message
   */
  addProducts(): void {
    if (this.selection.selected.length) {
      this.spinnerBtn = true;
      let newProducts = this.selection.selected.map(p => { return { productUuid: p.uuid, quantity: p.quantity } })
      this.cartService.addCartProduct(newProducts)
        .pipe(finalize(() => this.spinnerBtn = false))
        .pipe(map(() => {
          // Push to cart the new products
          let newCartProducts = this.cartService.cartProductsValue;
          newCartProducts.push(...this.selection.selected);
          this.cartService.setCartProductsValue(newCartProducts);
          // Remove all products from selection
          this.selection.selected.splice(0, this.selection.selected.length);
        }))
        .subscribe(() => {
          this.snackbarService.open('המוצרים נוספו בהצלחה לעגלה', 'success');
        }, () => this.snackbarService.open('אין אפשרות להוסיף מוצרים כרגע, נסה שוב מאוחר יותר', 'error'));
    }
  };
}