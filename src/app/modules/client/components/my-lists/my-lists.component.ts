import { Component, OnInit, ViewChild, DoCheck } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { MyList, StatusDelivery } from '@/shared/models/myList.model';
import { CartService } from '../../services/cart.service';
import { SnackbarService } from '@/core/services/snackbar.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'client-my-lists',
  templateUrl: './my-lists.component.html',
  styleUrls: ['./my-lists.component.scss']
})
export class MyListsComponent implements OnInit ,DoCheck {

  dataSource: MatTableDataSource<MyList>;
  pageSize: string = '10';
  displayedColumns: string[] = ['orderNumber', 'date', 'statusDelivery', 'totalPrice'];
  isLoadLists: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private cartService: CartService,
    private snackbarService: SnackbarService,
  ) {  
    this.cartService.getMyLists()
    .pipe(finalize(() => this.isLoadLists = true))
    .subscribe(lists => this.dataSource = new MatTableDataSource<MyList>(lists),
      () => this.snackbarService.open('אין אפשרות להציג את רשימות הקניות, נסה שוב מאוחר יותר', 'error'));
    }
    
    ngOnInit() {}

    ngDoCheck() {
      // Check if have paginator and is not set on dataSource and is bigger thaht pageSize
      if (this.paginator && !this.dataSource.paginator && this.dataSource.data.length >= parseInt(this.pageSize)) {
        // Call once
        this.dataSource.paginator = this.paginator;
      }
    }

  /**
   * Get status delivery from enum in hebrew
   * @param {StatusDelivery} status - status of list product
   * @return {string} - of status delivery
   */
  getStatusDelivery(status: StatusDelivery): string {
    return StatusDelivery[status];
  };
}