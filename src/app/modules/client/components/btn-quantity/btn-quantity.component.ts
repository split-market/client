import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CartService } from '@/modules/client/services/cart.service';
import { Product } from '@/shared/models/product.model';
import { SnackbarService } from '@/core/services/snackbar.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'shared-btn-quantity',
  templateUrl: './btn-quantity.component.html',
  styleUrls: ['./btn-quantity.component.scss']
})
export class BtnQuantityComponent implements OnInit {

  @Input() product: Product;
  @Output() removeProduct = new EventEmitter<boolean>(); // Emit if product remove or quantity small of zero
  @Output() change = new EventEmitter<boolean>(); // When quantity change

  disabled: boolean;
  currentQuantity: number = 0;

  constructor(
    private cartService: CartService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.currentQuantity = this.product.quantity;
    // };
  }
  /**
   * When input of quantity change
   * check valid number - if small of 1 - remove the product
   * if not update the product quantity and the total price
   */
  quantityChange(): void {
    this.disabled = true;
    this.change.emit(true);

    if (this.product.quantity < 1) {
      this.cartService.removeCartProduct(this.product.uuid)
        .subscribe(() => {
          this.removeProduct.emit();
          this.snackbarService.open('המוצר הוסר מהעגלה.', 'success', 1000);
        },
          () => {
            this.change.emit(false);
            this.disabled = false;
            this.product.quantity = this.currentQuantity;
            this.snackbarService.open('אין אפשרות למחוק את המוצרים שביקשת, נסה שוב מאוחר יותר', 'error');
          });
    } else {
      if (this.product.quantity > 99)
        this.product.quantity = 99;

      this.cartService.updateCartProduct(this.product)
        .pipe(finalize(() => this.disabled = false))
        .subscribe(() => {
          this.currentQuantity = this.product.quantity;
        },
          () => {
            this.change.emit(false);
            this.product.quantity = this.currentQuantity;
            this.snackbarService.open('אין אפשרות לעדכן את הכמות שביקשת, נסה שוב מאוחר יותר', 'error');
          });
    }
  };
  /**
   * Add to cart new product - send product uuid to server
   * Update the cart service - push new product
   * and set quantity and total price
   */
  addToCart(): void {
    this.disabled = true;
    this.cartService.addCartProduct(this.product)
      .subscribe(() => {
        this.disabled = false;
        this.currentQuantity = this.product.quantity;
        this.snackbarService.open('המוצר נוסף לעגלה', 'success', 1000);
      },
        () => {
          this.product.quantity--;
          this.removeProduct.emit(true);
          this.snackbarService.open('אין אפשרות להוסיף לעגלה כרגע, נסה שוב מאוחר יותר', 'error');
        });
  };
}
