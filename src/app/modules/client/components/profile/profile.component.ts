import { Component, OnInit } from '@angular/core';
import { User } from '@/shared/models/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '@/core/http/user.service';
import { finalize } from 'rxjs/operators';
import { SnackbarService } from '@/core/services/snackbar.service';
import { AuthenticationService } from '@/core/authentication/authentication.service';

@Component({
  selector: 'client-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileForm: FormGroup;
  user: User;
  avatarImgPath: string | ArrayBuffer;
  isLoadProfile: boolean;
  spinnerBtn: boolean;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.fetchProfile(this.authenticationService.getUser());
  }
  /**
   * Fetch the user in form group
   * @param {User} user - user basic info 
   */
  fetchProfile(user: User): void {
    this.user = user;
    this.profileForm = this.fb.group({
      firstName: [user.firstName, [Validators.required, Validators.minLength(3)]],
      lastName: [user.lastName, [Validators.required, Validators.minLength(3)]]
    });
    this.isLoadProfile = true;
  };
  /**
   * 
   */
  update(): void {
    if (this.profileForm.invalid || this.spinnerBtn) return;
    this.spinnerBtn = true;

    this.userService.update(this.profileForm.value)
      .pipe(finalize(() => this.spinnerBtn = false))
      .subscribe(() => alert('success'),
        () => this.snackbarService.open('אין אפשרות לעדכן את הפרופיל כרגע,נסה שוב מאוחר יותר', 'error'));
  };
  /**
   * Change avatar image and render on local page
   * @param {FileList} files FileList from input{type=file}
   */
  changeAvatar(files: FileList): void {
    if (files.length) {
      let file = files[0];
      var reader = new FileReader();
      reader.onloadend = () => {
        this.user.imgProfilePath = reader.result;
      };
      this.authenticationService.update(this.user);
      reader.readAsDataURL(file);
    }
  };
}
