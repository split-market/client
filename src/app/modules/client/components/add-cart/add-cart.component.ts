import { Component, OnInit, Input } from '@angular/core';
import { SnackbarService } from "@/core/services/snackbar.service";
import { CartService } from "../../services/cart.service";
import { AuthenticationService } from "@/core/authentication/authentication.service";
import { Product } from '@/shared/models/product.model';

@Component({
  selector: 'client-add-cart',
  templateUrl: './add-cart.component.html',
  styleUrls: ['./add-cart.component.scss']
})
export class AddCartComponent implements OnInit {

  @Input() product: Product;
  selected: boolean;
  disabled: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private cartService: CartService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    if (this.product.quantity > 0) {
        this.selected = true;
    }
  }

    /**
   * Add new product to cart
   * check valid user with role
   * send the product to server
   */
  addToCart(): void {
    this.disabled = true;
    this.selected = true;

    // Check if have user with any role if not call register function
    if (!this.authenticationService.isLogin()) {
      this.authenticationService
        .register()
        .subscribe(() => this.addCardProduct(),
          () => this.snackbarService.open("אין אפשרות להוסיף מוצר כרגע, נסה שוב מאוחר יותר","error"));
    } else {
      this.addCardProduct();
    }
  }
  /**
   * Set product on server cart
   * If success enable button quantity
   */
  addCardProduct(): void {
    this.product.quantity = 1;
    this.cartService.addCartProduct(this.product).subscribe(
      () => {
        this.disabled = false;
        this.snackbarService.open("המוצר נוסף לעגלה", "success", 1000);
      },
      () => {
        this.snackbarService.open(
          "אין אפשרות להוסיף לעגלה כרגע, נסה שוב מאוחר יותר",
          "error"
        );
        this.selected = false;
        this.disabled = false;
      }
    );
  }

}
