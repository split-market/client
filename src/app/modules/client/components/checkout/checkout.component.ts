import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from "@angular/forms";
import { AuthenticationService } from "@/core/authentication/authentication.service";
import { CartService } from "../../services/cart.service";

const REGEX_ISRAEL_PHONE = /^0\d([\d]{0,1})([-]{0,1})\d{7}$/;

const isValidIsraeliID = (control: AbstractControl) => {
  var id = String(control.value).trim();
  if (id.length > 9 || id.length < 5) return { isValidIsraeliID: true };
  id = id.length < 9 ? ("00000000" + id).slice(-9) : id;
  let isValid =
    Array.from(id, Number).reduce((counter, digit, i) => {
      const step = digit * ((i % 2) + 1);
      return counter + (step > 9 ? step - 9 : step);
    }) % 10 === 0;
  return isValid ? null : { isValidIsraeliID: true };
};

@Component({
  selector: "client-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.scss"]
})
export class CheckoutComponent implements OnInit {
  userForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private cartService: CartService
  ) {
    const currentUser = this.authenticationService.getUser();
    if (currentUser) {
      this.userForm = this.fb.group({
        firstName: [
          currentUser.firstName,
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20)
          ]
        ],
        lastName: [
          currentUser.lastName,
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20)
          ]
        ],
        phoneNumber: [
          currentUser.phoneNumber,
          [Validators.required, Validators.pattern(REGEX_ISRAEL_PHONE)]
        ],
        idCard: [currentUser.id_card, [Validators.required, isValidIsraeliID]],
        city: [currentUser.city, Validators.required],
        apartmentNumber: [
          currentUser.apartmentNumber || "",
          Validators.required
        ],
        floor: [currentUser.floor, Validators.required],
        addressStreet: [currentUser.addressStreet, Validators.required]
      });
    }
  }
  ngOnInit() {}

  /** TODO */
  checkout(): void {
    if (this.userForm.invalid) return;

    const user = this.userForm.value;
    this.authenticationService
      .update(user)
      .subscribe(
        user =>
          this.cartService
            .calculateMyList(user.uuid)
            .subscribe(console.log, console.log),
        console.log
      );
  }
}
