import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ClientComponent } from './pages/client.component';
import { CartComponent } from './components/cart/cart.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MyListsComponent } from './components/my-lists/my-lists.component';
import { ClientAuthGuard } from '@/core/guards/client-auth.guard';
import { MyListComponent } from './components/my-lists/my-list/my-list.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { Role } from '@/shared/models/role.model';
import { ShoppingComponent } from './components/shopping/shopping.component';

const routes: Routes = [
    {
        path: '',
        component: ClientComponent,
        children: [
            {
                path: '', component: HomeComponent
            },
            {
                path: 'shop/:id', component: ShoppingComponent
            },
            {
                path: 'cart', component: CartComponent
            },
            {
                path: 'profile', component: ProfileComponent,
                canActivate: [ClientAuthGuard],
                data: { roles: [Role.Customer] }
            },
            {
                path: 'my-lists', component: MyListsComponent,
                canActivate: [ClientAuthGuard],
                data: { roles: [Role.Customer] }
            },
            {
                path: 'my-list/:id', component: MyListComponent,
                canActivate: [ClientAuthGuard],
                data: { roles: [Role.Customer] }
            },
            {
                path: 'checkout', component: CheckoutComponent,
                canActivate: [ClientAuthGuard],
                data: { roles: [Role.Customer] }
            },
            {
                path: 'contact-us', component: ContactUsComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule { }
