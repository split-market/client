import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessComponent } from './pages/business.component';
import { BusinessAuthGuard } from '@/core/guards/business-auth.guard';
import { LoginComponent } from './components/login/login.component';
import { Role } from '@/shared/models/role.model';
import { OrdersComponent } from './components/orders/orders.component';
import { UserOrderComponent } from './components/orders/user-order/user-order.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MyShopComponent } from './components/my-shop/my-shop.component';
import { GlobalShopComponent } from './components/global-shop/global-shop.component';

const routes: Routes = [
    {
        path: '', component: BusinessComponent,
        canActivate: [BusinessAuthGuard],
        data: { roles: [Role.Seller] },
        children: [
            {
                path: 'orders', component: OrdersComponent
            },
            {
                path: 'shop/:id', component: MyShopComponent
            },
            {
                path: 'shop', component: GlobalShopComponent
            },
            {
                path: 'profile', component: ProfileComponent
            },
            {
                path: 'user-order/:id', component: UserOrderComponent
            }
        ]
    },
    {
        path: 'login', component: LoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BusinessRoutingModule { }
