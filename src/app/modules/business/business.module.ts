import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { BusinessRoutingModule }    from './business-routing.module';
import { BusinessComponent }        from './pages/business.component';
import { MaterialModule }           from '@/core/modules/material.module';
import { LoginComponent }           from './components/login/login.component';
import { MatInputModule, 
         MatButtonModule,
         MatSelectModule,
         MatRadioModule,
         MatCardModule }            from '@angular/material';
import { ReactiveFormsModule }      from '@angular/forms';
import { SharedModule }             from '@/shared/module/shared.module';
import { NavComponent }             from './components/nav/nav.component';
import { OrdersComponent }          from './components/orders/orders.component';
import { UserOrderComponent }       from './components/orders/user-order/user-order.component';
import { ProfileComponent }         from './components/profile/profile.component';
import { MyShopComponent }          from './components/my-shop/my-shop.component';
import { GlobalShopComponent }      from './components/global-shop/global-shop.component';
import { AddProductComponent }      from './components/add-product/add-product.component';
import { EditProductComponent }     from './components/edit-product/edit-product.component';

@NgModule({
    declarations: [
        BusinessComponent,
        LoginComponent,
        NavComponent,
        OrdersComponent,
        EditProductComponent,
        UserOrderComponent,
        ProfileComponent,
        AddProductComponent,
        MyShopComponent,
        GlobalShopComponent
    ],
    imports: [
        CommonModule,
        BusinessRoutingModule,
        MaterialModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatRadioModule,
        MatCardModule,
        ReactiveFormsModule,
        SharedModule
    ],
    entryComponents: [EditProductComponent, AddProductComponent]
})
export class BusinessModule { }
