import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { environment } from 'environments/environment.prod';
import { Shop } from '@/shared/models/shop.model';
import { tap } from 'rxjs/operators';
import { Product } from '@/shared/models/product.model';
import { MyList } from '@/shared/models/myList.model';
import { User } from '@/shared/models/user.model';

const currentShopSeller = 'currentShopSeller';

@Injectable({
  providedIn: 'root'
})
export class ShopSellerService {

    private currentShopSellerSubject = new BehaviorSubject<Shop>(JSON.parse(localStorage.getItem('currentShopSeller')));
    // public currentShopSeller = this.currentShopSellerSubject.asObservable();

    constructor(
        private http: HttpClient
    ) {
    }
    /**
     * Get shop value on behavior subject
     * @return shop value
     */
    public get currentSellerValue(): Shop{
        return this.currentShopSellerSubject.value;
    };
    /**
     * Post new shop
     * @param {Shop} Shop 
     * @returns {Shop} - shop created
     */
    create(shop: Shop): Observable<Shop> {
        const href = `${environment.shop}`;
        return this.http.post<Shop>(href, shop)
            .pipe(tap((shop => {
                localStorage.setItem(currentShopSeller, JSON.stringify(shop));
            })));
    };
    /**
     * Get shop by user seller
     * @param {Shop} Shop 
     * @returns {Shop} - shop
     */
    get(UserUuid: string): Observable<Shop> {
        console.log(UserUuid)
        const href = `${environment.shop}`;
        return this.http.get<Shop>(href + `/${UserUuid}`)
            .pipe(tap((shop => {
                localStorage.setItem(currentShopSeller, JSON.stringify(shop));
                this.currentShopSellerSubject.next(JSON.parse(localStorage.getItem('currentShopSeller')));
            })));
    };
    /**
     * Get orders by seller
     * @param {Shop} ShopUuid 
     * @param {String} statusDelivery status of delivery
     * @returns {Orders} - All orders for shop
     */
    getOrders(ShopUuid: string, statusDelivery: string): Observable<MyList[]> {
        const href = `${environment.shopList}`;
        return this.http.get<MyList[]>(href, { params: {ShopUuid, statusDelivery}});
    };
    /**
    * Get products list order number
    * @param {string} numberOrder - uuid of list
    * @param {string} ShopUuid - uuid of shop
    * @return {Observable<Product[]>} products of this list
    */
    getListOrderById(ShopUuid: string,numberOrder: string): Observable<any> {
        const href = `${environment.myList}`;
        return this.http.get<any>(href + `/${numberOrder}`, {params: {ShopUuid}});
    };
    /**
    * Get user customer
    * @param {string} UserUuid - uuid of customer
    * @return {Observable<User>} customer
    */
    getCustomer(UserUuid: string): Observable<User> {
        const href = `${environment.user}`;
        return this.http.get<User>(href + `/${UserUuid}`);
    };
    /**
     * Update product of shop by seller
     * @param {ProductDetails} product with details   
     * @param {ShopUuid} uuid  
     * @param {ProductUuid} uuid 
     * @returns {Product} - product
     */
    updateProduct(ProductDetails: Product , ShopUuid: string, ProductUuid: string): Observable<Product> {
        const href = `${environment.productDetails}`;
        return this.http.put<Product>(href, ProductDetails, { params: {ShopUuid, ProductUuid}})
    };
    /**
     * add product of to curent shop
     * @param {ProductDetails} product with details   
     * @param {ShopUuid} uuid  
     * @param {ProductUuid} uuid 
     * @returns {Product} - product
     */
    addProduct(ProductDetails: Product , ShopUuid: string, ProductUuid: string): Observable<Product> {
        const href = `${environment.productDetails}`;
        let product = Object.assign({}, ProductDetails, {ShopUuid} , {ProductUuid});
        return this.http.post<Product>(href, product)
    };
    /**
     * search product of to curent shop 
     * @param {String} term  
     * @param {String} ShopUuid 
     * @returns {Product} - product
     */
    searchProduct(term: string, ShopUuid?: string): Observable<any> {
        return this.http.get<any>(environment.search + term,  {params: ShopUuid ?{ShopUuid}:{}});
    };
    /**
     * Update product of shop by seller
     * @param {ProductDetails} product with details   
     * @param {ShopUuid} uuid  
     * @param {ProductUuid} uuid 
     * @returns {Product} - product
     */
    updateStatusDelivery(statusDelivery: string , UserUuid: string): Observable<any>{
        const href = `${environment.shopList}`;
        return this.http.put<any>(href, {statusDelivery:statusDelivery}, { params: {UserUuid}})
    };
  }