import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShopSellerService } from '../../services/shopSeller.service';

@Component({
  selector: 'business-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor( 
    private shopSellerService: ShopSellerService) {}

  ngOnInit() {}
}
