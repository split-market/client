import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { Login } from '@/shared/models/login.model';
import { finalize } from 'rxjs/operators';
import { ShopSellerService } from '../../services/shopSeller.service';
import { Role } from '@/shared/models/role.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'business-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isInvalidLogin: boolean;
  spinnerBtn: boolean;

  constructor(
    private fb: FormBuilder,
    private shopSellerService: ShopSellerService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      role: [Role.Seller]
    });
  }
  ngOnInit() {
  }
  /**
   * Chck if form is valid and is not in load req mode
   * show spinner load and send req to service
   */
  login(): void {
    if (this.loginForm.invalid || this.spinnerBtn) return;

    this.isInvalidLogin = false;
    this.spinnerBtn = true;

    let user: Login = this.loginForm.value;
    this.authenticationService.login(user)
        .pipe(finalize(() => this.spinnerBtn = false))
        .subscribe((user) =>{
          this.shopSellerService.get(user.uuid)
          .subscribe((shop) =>{
            this.router.navigate(['business']);
          })
        },
            () => this.isInvalidLogin = true);
};

}
