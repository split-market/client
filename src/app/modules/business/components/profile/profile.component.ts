import { Component, OnInit } from '@angular/core';
import { ShopSellerService } from '../../services/shopSeller.service';
import { Shop } from '@/shared/models/shop.model';
import { User } from '@/shared/models/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@/core/authentication/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  shopForm: FormGroup;
  isLoadProfile = false;
  isLoadShop = false;
  spinnerBtn = false;
  
  constructor(
    private fb: FormBuilder,
    private shopSellerService: ShopSellerService,
    private authenticationService: AuthenticationService,
  ) {
    this.fetchShop(this.shopSellerService.currentSellerValue);
    this.fetchProfile(this.authenticationService.getUser());
   }

  ngOnInit() {
  }
  /**
   * Fetch the user in form group
   * @param {User} user - user basic info 
   */
  fetchProfile(user: User): void {
    this.profileForm = this.fb.group({
      firstName: [user.firstName, [Validators.required, Validators.minLength(3)]],
      lastName: [user.lastName, [Validators.required, Validators.minLength(3)]],
      email: [user.email, [Validators.required]],
      phoneNumber: [user.phoneNumber, [Validators.required]]
    });
    this.isLoadProfile = true;
  };
  /**
   * Fetch the shop in form group
   * @param {Shop} shop - shop basic info 
   */
  fetchShop(shop: Shop){
    // this.currentShopSellerSubject = shop;
    this.shopForm = this.fb.group({
      // FreeShipping: [shop.FreeShipping, [Validators.required, Validators.minLength(3)]],
      address: [shop.address, [Validators.required, Validators.minLength(3)]],
      city: [shop.city, [Validators.required, Validators.minLength(3)]],
      distributionRange: [shop.distributionRange, [Validators.required]],
      name: [shop.name, [Validators.required]],
      phoneNumber: [shop.phoneNumber, [Validators.required]],
      deliveryTime: [shop.deliveryTime, [Validators.required]],
      priceShipping: [shop.priceShipping, [Validators.required]]
    });
    this.isLoadShop = true;
  }
}
