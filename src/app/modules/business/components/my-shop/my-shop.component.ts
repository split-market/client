import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Product } from '@/shared/models/product.model';
import { EditProductComponent } from '../edit-product/edit-product.component';

@Component({
  selector: "app-my-shop",
  templateUrl: "./my-shop.component.html",
  styleUrls: ["./my-shop.component.scss"]
})
export class MyShopComponent implements OnInit {

  constructor(
    private dialog: MatDialog
    ) {}

  ngOnInit() {}

  editProduct(product: Product): void {
    this.dialog.open(EditProductComponent, {
      autoFocus: false,
      panelClass: "full-dialog",
      data: { selected: product }
    });
  }
}
