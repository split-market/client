import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Product } from '@/shared/models/product.model';
import { AddProductComponent } from '../add-product/add-product.component';

@Component({
  selector: 'app-global-shop',
  templateUrl: './global-shop.component.html',
  styleUrls: ['./global-shop.component.scss']
})
export class GlobalShopComponent implements OnInit {

  constructor(
    private dialog: MatDialog
    ) {}

  ngOnInit() {}

  addProduct(product: Product): void {
    this.dialog.open(AddProductComponent, {
      autoFocus: false,
      panelClass: "full-dialog",
      data: { selected: product }
    });
  }
}