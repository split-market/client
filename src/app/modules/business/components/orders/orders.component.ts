import { Component, OnInit } from '@angular/core';
import { ViewChild, DoCheck } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatRadioGroup } from '@angular/material';
import { MyList, StatusDelivery } from '@/shared/models/myList.model';
import { SnackbarService } from '@/core/services/snackbar.service';
import { finalize } from 'rxjs/operators';
import { ShopSellerService } from '../../services/shopSeller.service';
import { Shop } from '@/shared/models/shop.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, DoCheck {
  dataSource: MatTableDataSource<MyList>;
  pageSize: string = '10';
  displayedColumns: string[] = ['orderNumber', 'date', 'statusDelivery', 'totalPrice'];
  isLoadLists = false;
  seasons: string[] = ['active', 'ends']
  @ViewChild(MatPaginator) paginator: MatPaginator;
  currentShopSellerSubject: Shop;
  
  constructor(
    private snackbarService: SnackbarService,
    private shopSellerService: ShopSellerService
    ) {
      this.currentShopSellerSubject = this.shopSellerService.currentSellerValue;
      this.shopSellerService.getOrders(this.currentShopSellerSubject.uuid, 'active')
      .pipe(finalize(() => this.isLoadLists = true))
      .subscribe(lists => this.dataSource = new MatTableDataSource<MyList>(lists),
        () => this.snackbarService.open('אין אפשרות להציג את רשימות הקניות, נסה שוב מאוחר יותר', 'error'));
    }
    
  ngOnInit() {}

  ngDoCheck() {
    // Check if have paginator and is not set on dataSource and is bigger thaht pageSize
    if (this.paginator && !this.dataSource.paginator && this.dataSource.data.length >= parseInt(this.pageSize)) {
      // Call once
      this.dataSource.paginator = this.paginator;
    }
  }
  /**
   * Get orders by options active or ends
   * @param {MatRadioGroup} event - event of MatRadioGroup
   */
  radioChange(event: MatRadioGroup): void{
    this.shopSellerService.getOrders(this.currentShopSellerSubject.uuid, event.value)
    .pipe(finalize(() => this.isLoadLists = true))
    .subscribe(lists => this.dataSource = new MatTableDataSource<MyList>(lists),
      () => this.snackbarService.open('אין אפשרות להציג את רשימות הקניות, נסה שוב מאוחר יותר', 'error'));
    
  }
  /**
   * Get status delivery from enum in hebrew
   * @param {StatusDelivery} status - status of list product
   * @return {string} - of status delivery
   */
  getStatusDelivery(status: StatusDelivery): string {
    return StatusDelivery[status];
  };

}
