import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShopSellerService } from '@/modules/business/services/shopSeller.service';
import { User } from '@/shared/models/user.model';
import { Product } from '@/shared/models/product.model';
import { SnackbarService } from '@/core/services/snackbar.service';
// import { BreakpointObserver } from '@angular/cdk/layout';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-order',
  templateUrl: './user-order.component.html',
  styleUrls: ['./user-order.component.scss']
})
export class UserOrderComponent implements OnInit {
  spinnerBtn: boolean;
  orderNumber: number;
  customer: User;
  UserUuid: string;
  products: Product[];
  isProductsActive: boolean = false;
  displayedColumns = ['product', 'price', 'quantity'];

  // isHandset$: Observable<boolean> =
  // this.breakpointObserver.observe(['(max-width: 767.98px)'])
  //     .pipe(map(result => {
  //         return result.matches;
  //     }));
  
  constructor(
    private route: ActivatedRoute,
    private snackbarService: SnackbarService,
    private shopSellerService: ShopSellerService,
    // private breakpointObserver: BreakpointObserver
  ) { }

  ngOnInit() {
    this.spinnerBtn = false;
    const currentShopSellerSubject = this.shopSellerService.currentSellerValue;
    this.route.params.subscribe(value => {
      this.shopSellerService.getListOrderById(currentShopSellerSubject.uuid , value.id)
      .subscribe((data) => {
        this.products = data.products;
        this.UserUuid = data.UserUuid;
        this.isProductsActive = data.statusDelivery === 'ends'? false: true;
        //Get customer by uuid of customer
        this.shopSellerService.getCustomer(this.UserUuid)
        .subscribe((customer) => {
          this.spinnerBtn = true;
          this.customer = customer;
        },() => this.snackbarService.open('אין אפשרות להציג את הלקוח, נסה שוב מאוחר יותר', 'error'));
      },() => this.snackbarService.open('אין אפשרות להציג את רשימות הקניות, נסה שוב מאוחר יותר', 'error'));
    });
  }
  //Update status Delivery to ends
  finish(): void{
    this.shopSellerService.updateStatusDelivery('ends', this.UserUuid)
    .subscribe((a) => this.snackbarService.open('סיום הזמנה התבצע בהצלחה', 'success')
    ,() => this.snackbarService.open('אין אפשרות לסיים משלוח נסה יותר מאוחר', 'error'))
  }
}
