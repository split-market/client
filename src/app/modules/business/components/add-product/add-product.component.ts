import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ShopSellerService } from '@/modules/business/services/shopSeller.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from '@/shared/models/product.model';

const REGEX_NUMBER = /^[0-9]+(\.?[0-9]+)?$/;

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  product: Product;
  productForm: FormGroup;
  spinnerBtn: boolean;
  disabledBtn = true;

  constructor(
    private fb: FormBuilder,
    private shopSellerService: ShopSellerService,
    private dialogRef: MatDialogRef<AddProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.product = data.selected;

    this.productForm = this.fb.group({
      saleFinish: [this.product.saleFinish || null],
      price: [this.product.price|| null, [Validators.required, Validators.pattern(REGEX_NUMBER)]],
      salePrice: [this.product.salePrice || null, [Validators.pattern(REGEX_NUMBER)]],
      saleQuantity: [this.product.saleQuantity|| null, [Validators.pattern(REGEX_NUMBER)]],
    });
  }

  ngOnInit() {
    this.productForm.valueChanges.subscribe((product) => {
      (product.saleFinish && product.saleQuantity) || 
      (product.price != "" && product.price && 
      !(product.saleFinish || product.saleQuantity || product.salePrice)) ? this.disabledBtn = false:  this.disabledBtn = true;
    })
  }

  addProduct(): void {
    if (this.productForm.invalid || this.spinnerBtn || this.disabledBtn) return;
    const currentShopSellerSubject = this.shopSellerService.currentSellerValue;
    
    this.shopSellerService.addProduct(
      this.productForm.value, currentShopSellerSubject.uuid, this.product.uuid
      )
      .subscribe((product) => {
        console.log(product, "AAAAPPPP");
        this.closeDialog();
      }, (err) => {
        console.log(err, "ERRRRPPPP");
      })
  }

  /**
   * Close edit in dialog
   */
  closeDialog(): void {
    this.dialogRef.close();
  };
}
