import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Product } from '@/shared/models/product.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShopSellerService } from '../../services/shopSeller.service';

const REGEX_NUMBER = /^[0-9]+(\.?[0-9]+)?$/;

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  product: Product;
  productForm: FormGroup;
  spinnerBtn: boolean;
  disabledBtn = true;

  constructor(
    private fb: FormBuilder,
    private shopSellerService: ShopSellerService,
    private dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.product = data.selected;

    this.productForm = this.fb.group({
      saleFinish: [this.product.saleFinish],
      price: [this.product.price, [Validators.required, Validators.pattern(REGEX_NUMBER)]],
      salePrice: [this.product.salePrice, [Validators.pattern(REGEX_NUMBER)]],
      saleQuantity: [this.product.saleQuantity, [Validators.pattern(REGEX_NUMBER)]],
    });
  }

  ngOnInit() {
    this.productForm.valueChanges.subscribe((product) => {
      (product.salePrice != "" && product.salePrice) && 
      (product.saleFinish && product.saleQuantity) || 
      (product.price != "" && product.price && 
      !(product.saleFinish || product.saleQuantity || product.salePrice)) ? this.disabledBtn = false:  this.disabledBtn = true;
    })
  }

  updateProduct(): void {
    if (this.productForm.invalid || this.spinnerBtn || this.disabledBtn) return;
    const currentShopSellerSubject = this.shopSellerService.currentSellerValue;

    this.shopSellerService.updateProduct(
      this.productForm.value, currentShopSellerSubject.uuid, this.product.uuid
    )
      .subscribe((product) => {
        console.log(product);
      }, (err) => {
        console.log(err);
      })
  }

  /**
   * Close edit in dialog
   */
  closeDialog(): void {
    this.dialogRef.close();
  };
}
