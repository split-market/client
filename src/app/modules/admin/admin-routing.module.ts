import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './pages/admin.component';
import { LoginComponent } from './components/login/login.component';
import { AdminAuthGuard } from '@/core/guards/admin-auth.guard';
import { Role } from '@/shared/models/role.model';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SignInBusinessComponent } from './components/sign-in-business/sign-in-business.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminAuthGuard],
    data: { roles: [Role.Admin] },
    children: [
      {
        path: '', component: DashboardComponent,
      },
      {
        path: 'business', component: SignInBusinessComponent,
      },
    
    ]
  },
  {
    path: 'login', component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
