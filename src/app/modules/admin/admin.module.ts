import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './pages/admin.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MaterialModule } from '@/core/modules/material.module';
import { SharedModule } from '@/shared/module/shared.module';
import { NavComponent } from './components/nav/nav.component';
import { MassagesComponent } from './components/massages/massages.component';
import { SignInBusinessComponent } from './components/sign-in-business/sign-in-business.component';

@NgModule({
  declarations: [
    AdminComponent,
    LoginComponent,
    DashboardComponent,
    NavComponent,
    MassagesComponent,
    SignInBusinessComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class AdminModule { }
