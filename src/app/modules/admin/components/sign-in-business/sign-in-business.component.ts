import { finalize } from 'rxjs/operators';
import { MatStepper } from '@angular/material';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Role } from '@/shared/models/role.model';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { Router } from '@angular/router';
import { ShopAdminServicesService } from '../../services/shop-admin-services.service';

const REGEX_PASSWORD = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$";
const REGEX_PHONE_NUMBER = /^((\+972|972)|0)( |-)?([1-468-9]( |-)?\d{7}|(5|7)[0-9]( |-)?\d{7})$/;
const REGEX_NUMBER = /^[0-9]+(\.?[0-9]+)?$/;

@Component({
  selector: 'app-sign-in-business',
  templateUrl: './sign-in-business.component.html',
  styleUrls: ['./sign-in-business.component.scss']
})
export class SignInBusinessComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  registerForm: FormGroup;
  shopForm: FormGroup;
  spinnerBtn: boolean;
  isInvalidRegister: boolean;
  longitude: string;
  latitude: string;
  userUuid: string;

  constructor(
      private fb: FormBuilder,
      private router: Router,
      private authenticationService: AuthenticationService,
      private shopAdminServicesService: ShopAdminServicesService
  ) {
    // Rregister user seler
    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(REGEX_PASSWORD)]],
      role: [Role.Seller]
    });
   
    //Shop delails
    this.shopForm = this.fb.group({
      name: [null, Validators.required],
      address: [null, Validators.required],
      city: [null, Validators.required],
      postalCode: [null, [Validators.required,
                          Validators.minLength(5),
                          Validators.maxLength(5)]],
      distributionRange: [null, [Validators.required, Validators.pattern(REGEX_NUMBER)]],
      deliveryTime: [null, Validators.required],
      phoneNumber: [null, [Validators.required, Validators.pattern(REGEX_PHONE_NUMBER)]],
      priceShipping: [null]
    });
  }
  ngOnInit() {}
  /**
   * User seler register
   */
  register(): void {
    if (this.registerForm.invalid || this.spinnerBtn) return;
    this.isInvalidRegister = false;
    this.spinnerBtn = true;
    this.authenticationService.register(this.registerForm.value)
    .pipe(finalize(() => this.spinnerBtn = false))
      .subscribe((user) => {
        if(user)this.stepper.next();
        this.userUuid = user.uuid;
        console.log(user, "REEEEEE");
      },
      () => {
        this.isInvalidRegister = true;
        this.spinnerBtn = false;
    });
  };

  createShop(): void{
    debugger
    if (this.shopForm.invalid || this.spinnerBtn || !(this.longitude || this.latitude)) return;

    this.isInvalidRegister = false;
    this.spinnerBtn = true;
    this.shopAdminServicesService.create(Object.assign(
      {longitude: this.longitude, latitude: this.latitude},
      this.shopForm.value, {UserUuid: this.userUuid}))
    .pipe(finalize(() => this.spinnerBtn = false))
      .subscribe((shop) => {
        if(shop)this.stepper.next();
        console.log(shop, "SSSSHHHHOOOPPPPPP");
      },
      () => {
        this.isInvalidRegister = true;
        this.spinnerBtn = false;
    });
  }
  setLocation(event): void{
    console.log(event, "EEEEE");
      this.longitude = event.geometry.location.lng();
      this.latitude = event.geometry.location.lat();
  }

  finish(): void{
        this.router.navigate(['business']);
  }
}