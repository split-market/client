import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@/core/authentication/authentication.service';
import { finalize } from 'rxjs/operators';
import { Login } from '@/shared/models/login.model';
import { Router } from '@angular/router';
import { Role } from '@/shared/models/role.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isInvalidLogin: boolean;
  spinnerBtn: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService
) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      role: [Role.Admin]
    });
}
  ngOnInit() {
  }

     /**
     * Chck if form is valid and is not in load req mode
     * show spinner load and send req to service
     */
    login(): void {
      if (this.loginForm.invalid || this.spinnerBtn) return;

      this.isInvalidLogin = false;
      this.spinnerBtn = true;

      let user: Login = this.loginForm.value;
      this.authenticationService.login(user)
          .pipe(finalize(() => this.spinnerBtn = false))
          .subscribe((a) =>{
            console.log(a,"SSSSS")
            this.router.navigate(['/admin']);
          },
              () => this.isInvalidLogin = true);
  };
}
