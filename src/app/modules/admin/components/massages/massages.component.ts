import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

export interface PeriodicElement {
  name: string;
  email: string;
  subject: string;
  message:string,
}

const ELEMENT_DATA: PeriodicElement[] = [
  {email: 'ad123@gmail.com', name: 'Hydrogen', subject:'בלה בלה', message: 'H'},
  {email: 'ad153@gmail.com', name: 'Helium', subject: 'גם כאן', message: 'He'},
  {email: 'ad12rtg3@gmail.com', name: 'Lithium', subject: 'למה', message: 'Li'}
]

@Component({
  selector: 'app-massages',
  templateUrl: './massages.component.html',
  styleUrls: ['./massages.component.scss']
})
export class MassagesComponent implements OnInit {
  displayedColumns: string[] = ['email', 'name', 'subject', 'message'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  constructor() { }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  ngOnInit() {
  }

}
