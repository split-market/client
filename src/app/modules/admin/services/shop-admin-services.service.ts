import { Injectable } from '@angular/core';
import { Shop } from '@/shared/models/shop.model';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopAdminServicesService {

  constructor(
    private http: HttpClient
  ) { }
  /**
     * Post new shop
     * @param {Shop} Shop 
     * @returns {Shop} - shop created
     */
    create(shop: Shop): Observable<Shop> {
      const href = `${environment.shop}`;
      return this.http.post<Shop>(href, shop);
  };
}
