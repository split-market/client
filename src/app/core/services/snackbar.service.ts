import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

type snackbarType = 'success' | 'error' | 'warning';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(
    private snackbar: MatSnackBar
  ) { }

  /**
   * Open snackbar material with custom options
   * @param {string} message - Message of snakbar
   * @param {string} action - String of button that close the snakbar
   * @param {string} type - Typs of snackbar 'success' 'error' 'warning'
   * @param {number} duration - Duration time to close the snakbar
   */
  open(message: string, type?: snackbarType, duration?: number, action?: string): void {
    this.snackbar.open(message, action || 'X', {
      duration: duration || 4500,
      panelClass: ['custom-snackbar', `${type}`]
    });
  };
  /**
   * Open snackbar material with global error
   */
  openGlobalError(): void {
    this.snackbar.open('אין אפשרות כרגע להציג את המידע הרצוי, נסה שוב מאוחר יותר', 'X', {
      panelClass: ['custom-snackbar', 'error'],
      horizontalPosition: 'center'
    });
  };
}
