import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'environments/environment.prod';
import { Login } from '../../shared/models/login.model';
import { Register } from '@/shared/models/register.model';
import { User } from '@/shared/models/user.model';
import { Router } from '@angular/router';
import { CartService } from '@/modules/client/services/cart.service';
import { Status } from '@/shared/models/cart.model';

const credentialsKey = 'currentUser';

@Injectable()
export class AuthenticationService {

    private currentUserSubject = new Subject<User>();

    constructor(
        private http: HttpClient,
        private router: Router,
        private cartService: CartService
    ) {
    }
    /**
     * Set user value on subject
     * @return {User} Observable of user value
     */
    public get currentUserValue(): Observable<User> {
        return this.currentUserSubject.asObservable();
    };
    /**
     * Emit new user in subject
     * @param {User} user - user emit on subjct 
     */
    public setCurrentUserValue(user: User): void {
        this.currentUserSubject.next(user);
    };
    /**
     * If login success save the user on storage for example
     * @param {Login} loginData the data of login input form
     * @return {User} - user is login
     */
    login(loginData: Login): Observable<User> {
        const href = `${environment.login}`;
        const { email, password, role } = loginData;
        return this.http.get<User>(href, { params: { email, password, role } })
            .pipe(tap((user) => {
                localStorage.setItem(credentialsKey, JSON.stringify(user));
                this.cartService.getCart(Status.Pending)
                    .subscribe(() => this.currentUserSubject.next(user));
            }));
    };
    /**
     * Post new user
     * @param {Register} registerData 
     * @returns {User} - user created
     */
    register(registerData?: Register): Observable<User> {
        const href = `${environment.user}`;
        return this.http.post<User>(href, registerData)
            .pipe(tap((user => {
                if (user.role !== 'seller') {
                    localStorage.setItem(credentialsKey, JSON.stringify(user));
                    this.setCurrentUserValue(user);
                }
            })));
    };
    /**
     * Update new user
     * @param {Register| User} update guest to register or user details 
     * @returns {User} - user created
     */
    update(user?: Register | User): Observable<User> {
        const href = `${environment.user}`;
        return this.http.put<User>(href, user)
            .pipe(tap((user) => {
                localStorage.setItem(credentialsKey, JSON.stringify(user));
                this.setCurrentUserValue(user);
            }));
    };
    /**
     * Logout user
     * remove currentUser form storage
     */
    logout(): void {
        localStorage.removeItem(credentialsKey);
        this.router.navigate(['/']);
        this.currentUserSubject.next(null);
    };
    /**
     * Return if user is login 
     * @return {boolean} - if user is exist on storage
     */
    isLogin(): boolean {
        return localStorage.getItem(credentialsKey) ? true : false;
    };
    /**
     * Get token of user from storage if have current user
     * @return {string} - token of user 
     */
    getToken(): string {
        const savedCredentials = this.getUser();
        return savedCredentials && savedCredentials['token'];
    };
    /**
     * Get email of user from storage if have current user
     * @return {string} - Email of user
     */
    getEmail(): string {
        const savedCredentials = this.getUser();
        return savedCredentials && savedCredentials['email'];
    };
    /**
     * Get user role if have user 
     * @returns {string } - string role of user
     */
    getUserType(): string {
        const savedCredentials = JSON.parse(localStorage.getItem(credentialsKey));
        return savedCredentials['role'];
    };
    /**
     * Get the current user exist on storage 
     * @return {User} - current user
     */
    getUser(): User {
        const savedCredentials = localStorage.getItem(credentialsKey);
        return JSON.parse(savedCredentials);
    };
}
