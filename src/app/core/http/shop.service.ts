import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "environments/environment.prod";
import { Tag } from "@/shared/models/tags.model";
import { Category } from "@/shared/models/category.model";
import { Shop } from "@/shared/models/shop.model";
import { Location } from "@/shared/models/Location.model";
import { Product } from "@/shared/models/product.model";
import { ProductFilter } from "@/shared/models/productFilters.model";

@Injectable()
export class ShopService {
  constructor(private http: HttpClient) {}

  /**
   * Get shops by location of user
   * @returns {Observable<Shop[]>} - Array of shops
   */
  public getShopByLocation(coords: Location): Observable<Shop[]> {
    const params = new HttpParams()
      .set("latitude", coords.lat.toString())
      .set("longitude", coords.lng.toString());

    const href = `${environment.shop}`;
    return this.http.get<Shop[]>(href, { params });
  }
  /**
   * Get all categories with subcategiries
   * @return {Observable<Category[]>} - Array of categories
   */
  public getCategories(): Observable<Category[]> {
    const href = `${environment.category}`;
    return this.http.get<Category[]>(href);
  }
  /**
   * Create HttpParams with filters options
   * by category or tags or limit and offset
   * @param {productFilter} - Filters products
   * @return {Product[]} - Array of products
   */
  public getProductByShop(
    ShopUuid: string,
    productFilter: ProductFilter
  ): Observable<{ count: number; products: Product[] }> {
    let httpParams = new HttpParams();

    if (ShopUuid) httpParams = httpParams.append("ShopUuid", ShopUuid);

    Object.keys(productFilter).forEach(
      key => (httpParams = httpParams.append(key, productFilter[key]))
    );

    const href = `${environment.product}`;
    return this.http.get<any>(href, { params: httpParams });
  }

  public search(
    ShopUuid: string,
    term: string
  ): Observable<{ count: number; products: Product[] }> {
    const href = `${environment.search}`;
    return this.http.get<any>(`${href}${term}`, { params: { ShopUuid } });
  }

  searchAutocomplete(ShopUuid: string, term: string): Observable<any> {
    const href = `${environment.autocomplete}`;
    return this.http.get<any>(`${href}${term}`, { params: { ShopUuid } });
  }
  /**
   * Get all tags by subcategory uuid
   * @param {string} uuid uuid of subcategory
   * @return {tag[]} - array of tags stirng by subcategory
   */
  public getTags(SubCategoryUuid: string): Observable<Tag[]> {
    const href = `${environment.tags}`;
    return this.http.get<Tag[]>(href, {
      params: { SubCategoryUuid }
    });
  }
}
