import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@/shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment.prod';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) { }

  /**
   * Update user profile
   * @param {User} user
   */
  update(user: User): Observable<User> {
    const herf = `${environment.user}`;
    return this.http.put<User>(herf, user)
      .pipe(tap(user => this.authenticationService.update(user)));
  }
};