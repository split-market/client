import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpTokenInterceptor } from '../interceptors/http.token.interceptor';
import { ApiPrefixInterceptor } from '../interceptors/api-prefix.interceptor';
import { ClientAuthGuard } from '../guards/client-auth.guard';
import { BusinessAuthGuard } from '../guards/business-auth.guard';
import { MatSnackBarModule } from '@angular/material';
import { SnackbarService } from '../services/snackbar.service';
import { ShopService } from '../http/shop.service';
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { ClientCartGuard } from '../guards/client-cart.guard';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  exports: [
    HttpClientModule
  ],
  providers: [
    AuthenticationService,
    ShopService,
    SnackbarService,
    AdminAuthGuard,
    ClientAuthGuard,
    ClientCartGuard,
    BusinessAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiPrefixInterceptor,
      multi: true,
    },
  ]
})
export class CoreModule { }
