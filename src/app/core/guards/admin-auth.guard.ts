import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { CartService } from '@/modules/client/services/cart.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot) {
        const currentUser = this.authenticationService.getUser();
        if (currentUser && this.authenticationService.isLogin()) {
            // check if route is restricted by role
            if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
                this.router.navigate(['/admin/login']);
                return false;
            }
            return true;
        } else
            this.router.navigate(['/admin/login']);
        return false;
    }
}