import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { AuthenticationService } from "../authentication/authentication.service";
import { CartService } from "@/modules/client/services/cart.service";
import { Status } from "@/shared/models/cart.model";
import { map } from "rxjs/operators";

@Injectable()
export class ClientCartGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private cartService: CartService
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    if (this.authenticationService.isLogin()) {
      return this.cartService.getCart(Status.Pending).pipe(map(() => true));
    }
    return true;
  }
}
