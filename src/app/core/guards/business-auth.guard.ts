import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';



@Injectable()
export class BusinessAuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot) {
    const currentUser = this.authenticationService.getUser();
    if (currentUser) {
      // check if route is restricted by role
      //TODO auth
      if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
        this.router.navigate(['business/login']);
        return false;
      }
      return true;
    }
    this.router.navigate(['business/login']);
    return false;
  }
}

