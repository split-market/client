import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientCartGuard } from './core/guards/client-cart.guard';

const routes: Routes = [
    {
        path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule'
    },
    {
        path: 'business', loadChildren: './modules/business/business.module#BusinessModule'
    },
    {
        path: '', loadChildren: './modules/client/client.module#ClientModule',
        canActivate: [ClientCartGuard]
    },
    {
        path: '**', redirectTo: 'auth/404',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
