
export const environment = {
  production: false,
  host: 'http://localhost:3000',
  login: '/login',
  user: '/users',
  category: '/category',
  subCategory: '/subCategory',
  product: '/product',
  tags: '/tags',
  cart: '/cart',
  myList: '/myList',
  productTags: '/product/tags',
  firstQuery: '/product/first',
  search: '/search?s=',
};